<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Register extends CI_Controller {

	function __construct() {
			// Call the Model constructor
			parent::__construct();
			$this->load->model('m_user');
      $this->load->model('m_masters');
	}

  public function index($value='')
  {
    if($this->session->userdata('user_sipar')['username'] != ''){
      redirect('home');
    }
    $data['jenis_anjing'] = $this->m_masters->get_all_jenis();
    $this->load->view('auth/user/signupuser', $data);
  }

  public function registeruser()
  {
    $this->form_validation->set_rules('username', 'Username', 'required|trim|max_length[100]|is_unique[user.username]');
		$this->form_validation->set_rules('first_name', 'First Name', 'required|trim|max_length[100]');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required|trim|max_length[100]');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|max_length[50]');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
		$this->form_validation->set_rules('retype_password', 'Retype Password', 'required|trim|max_length[50]|matches[password]');
		$this->form_validation->set_rules('phone', 'Phone', 'required|trim|max_length[100]');

    if ($this->form_validation->run() == TRUE) {
      $data['username'] = $this->input->post('username');
      $data['password'] = sha1($this->input->post('password'));
			$data['first_name'] = $this->input->post('first_name');
			$data['last_name'] = $this->input->post('last_name');
			$data['no_hp'] = $this->input->post('phone');
			$data['alamat'] = $this->input->post('alamat');
			$data['date_join'] = date('Y-m-d');

      //password user (dan admin) dienkripsi menggunakan sha1
      $result = $this->m_user->registeruser($data);
      if($result){
				$this->session->set_flashdata('message', 'Akun anda berhasil dibuat, silahkan login');
      }else{
        $this->session->set_flashdata('message', 'Akun anda tidak dapat ditemukan');
      }
    }else{
      $this->session->set_flashdata('message', validation_errors());
    }
    redirect('register');
  }
}
?>
