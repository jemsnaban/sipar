<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller {

	function __construct() {
			// Call the Model constructor
			parent::__construct();
			$this->load->model('m_user');
      $this->load->model('m_masters');
	}

  public function index($value='')
  {
    if($this->session->userdata('user_sipar')['username'] != ''){
      redirect('home');
    }
    $data['jenis_anjing'] = $this->m_masters->get_all_jenis();
    $this->load->view('auth/user/loginuser', $data);
  }

  public function authenticateuser()
  {
    $this->form_validation->set_rules('username', 'Username', 'required|trim|max_length[100]');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|max_length[50]');

    if ($this->form_validation->run() == TRUE) {
      $username = $this->input->post('username');
      $password = $this->input->post('password');

      //password user (dan admin) dienkripsi menggunakan sha1
      $result = $this->m_user->authenticateuser($username, sha1($password));
      if($result){
        $this->session->set_userdata('user_sipar', $result);
        redirect('home');
      }else{
        $this->session->set_flashdata('message', 'Akun anda tidak dapat ditemukan');
      }
    }else{
      $this->session->set_flashdata('message', validation_errors());
    }
    redirect('login');
  }

  // logout
	public function logout() {
		// log history
		//$this->m_login->user_log_leave(array($this->session->userdata('session_operator')['user_id']));
		// destroy the session
		$this->session->unset_userdata('user_sipar');
		redirect('login');
	}

	public function adminlogin($value='')
	{
			$id = $this->session->userdata('admin_sipar')['id_admin'];
			if($id != ''){
					redirect('admin');
			}
			$this->load->view('auth/admin/loginadmin');
	}

	public function authenticateadmin($value='')
	{
			$this->form_validation->set_rules('username', 'Username', 'required|trim|max_length[100]');
			$this->form_validation->set_rules('password', 'Password', 'required|trim|max_length[50]');

	    if ($this->form_validation->run() == TRUE) {
	      $username = $this->input->post('username');
	      $password = $this->input->post('password');

	      //password user (dan admin) dienkripsi menggunakan sha1
	      $result = $this->m_user->authenticateadmin($username, sha1($password));
	      if($result){
	      	$result['id_admin'] = $result['id_user'];
	        $this->session->set_userdata('admin_sipar', $result);
	        redirect('admin');
	      }else{
	        $this->session->set_flashdata('message', 'Akun anda tidak dapat ditemukan');
	      }
	    }else{
	      $this->session->set_flashdata('message', validation_errors());
	    }
	    redirect('admin');
	}

	// logout
	public function logoutadmin() {
		// log history
		//$this->m_login->user_log_leave(array($this->session->userdata('session_operator')['user_id']));
		// destroy the session
		$this->session->unset_userdata('admin_sipar');
		redirect('login/adminlogin');
	}
}
?>
