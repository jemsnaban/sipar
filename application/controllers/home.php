<?php
set_time_limit(0);
ini_set('memory_limit','2048M');

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
			// Call the Model constructor
			parent::__construct();
			$this->load->model('m_dog');
			$this->load->model('m_masters');
	}

	public function index()
	{
		$data['carousel'] = "false";
		$data['jenis_anjing'] = $this->m_masters->get_all_jenis();
		$data['iklan'] = $this->m_dog->get_all_adv();
		$data['hit'] = $this->m_dog->get_all_adv_hit();
		$data['most'] = $this->m_dog->get_all_most_adv();
		//echo json_encode($data);die();
		$this->load->view('welcome', $data);
	}

	public function sort($id='')
	{
		//if($id != '') echo $id;die();
		$data['carousel'] = "false";
		$data['sort'] = $id;
		$data['jenis_anjing'] = $this->m_masters->get_all_jenis();
		$data['iklan'] = $this->m_dog->get_all_adv($id);
		$data['hit'] = $this->m_dog->get_all_adv_hit();
		$data['most'] = $this->m_dog->get_all_most_adv();
		//echo json_encode($data);die();
		$this->load->view('welcome', $data);
	}

	public function detail($id_iklan)
	{
		if($id_iklan == '') redirect('home');
		$data['jenis_anjing'] = $this->m_masters->get_all_jenis();
		$data['detail'] = $this->m_dog->get_detail_iklan($id_iklan);
		$this->load->view('detail', $data);
	}

	public function pasangiklan($value='')
	{
		if($this->session->userdata('user_sipar')['username'] == ''){
      redirect('login');
    }
		$data['jenis_anjing'] = $this->m_masters->get_all_jenis();
		$data['lokasi'] = $this->m_masters->get_all_lokasi();
		$this->load->view('pasangiklan', $data);
	}

	public function uploadiklan()
	{
		$data['gambar'] = "";
		$data['video'] = "";
		$data['harga'] = $this->input->post('harga');
		$data['judul_iklan'] = $this->input->post('juduliklan');
		$data['deskripsi'] = $this->input->post('deskripsi');
		$data['id_lokasi'] = $this->input->post('lokasi');
		$data['id_jenis'] = $this->input->post('jenis_anjing');
		$data['id_user'] = $this->session->userdata('user_sipar')['id_user'];
		$data['tanggal_iklan'] = date('Y-m-d');
		$tanggallahir = $this->input->post('tanggal_lahir');

		$myDateTime = DateTime::createFromFormat('d-m-Y', $tanggallahir);
		$newDateString = $myDateTime->format('Y-m-d');

		$data['tanggal_lahir'] = $newDateString;

		//upload video
		if (isset($_FILES['video']['name']) && $_FILES['video']['name'] != '') {
        unset($config);
        $date = date("ymd");
        $configVideo['upload_path'] = './uploads/videos';
        $configVideo['max_size'] = '120000';
        $configVideo["allowed_types"] = "*";
        $configVideo['overwrite'] = TRUE;
        $configVideo['remove_spaces'] = TRUE;
        $video_name = $date.$_FILES['video']['name'];
        $configVideo['file_name'] = $video_name;

        $this->load->library('upload', $configVideo);
        $this->upload->initialize($configVideo);
        if(!$this->upload->do_upload('video')) {
            echo $this->upload->display_errors();
						die();
        }else{
            $videoDetails = $this->upload->data();
            $data['video']= $configVideo['file_name'];
            //$data['video_detail'] = $videoDetails;
					//	echo json_encode($data);
            //$this->load->view('movie/show', $data);
        }

    }else{
        //echo "Please select a file";
    }

		//upload stambum
		if (isset($_FILES['avatar']) && is_uploaded_file($_FILES['avatar']['tmp_name'])) {
	    //load upload class with the config, etc...
			unset($config);
			$config['upload_path'] = './uploads/images';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size'] = '6000';
			/**$config['max_width'] = '1024';
			$config['max_height'] = '768';**/
			$config['overwrite'] = 'TRUE';

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('avatar'))
	    {
	        // no file uploaded or failed upload
	        $error = array('error' => $this->upload->display_errors());
					var_dump($error);
	    }
	    else
	    {
				$detail = $this->upload->data();
				$data['gambar']= $detail['file_name'];
	    }
		}
		else{

		}

		//upload image tambahan
		$query = $this->m_dog->uploadiklan($data);
		if($query){
			$this->session->set_flashdata('message', 'Iklan Berhasil Diunggah');
		}else{
			$this->session->set_flashdata('message', 'Terdapat Kesalahan');
		}

	//	$id = $query['id_iklan'];
	//	print_r($query);

		try {
			$name_array = array();
	    $count = count($_FILES['userfile']['size']);
	    foreach($_FILES as $key=>$value)
	    for($s=0; $s<=$count-1; $s++) {
		      $_FILES['userfile']['name'] = $value['name'][$s];
		      $_FILES['userfile']['type']    = $value['type'][$s];
		      $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
		      $_FILES['userfile']['error']    = $value['error'][$s];
		      $_FILES['userfile']['size']    = $value['size'][$s];
		      if($this->upload->do_upload('userfile')){
			      $d = $this->upload->data();
						//echo $d['file_name'] . " ";
						$this->m_dog->insert_adv_image($query, $d['file_name']);
					}
	    }
	    //$names= implode(',', $name_array);
			//print_r($names);
		} catch (Exception $e) {

		}

		redirect('home/pasangiklan');
	}

	public function jenis($id_jenis)
	{
		$data['carousel'] = "false";
		$data['jenis_anjing'] = $this->m_masters->get_all_jenis();
		$data['iklan'] = $this->m_dog->get_adv_per_jenis($id_jenis);
		$data['hit'] = $this->m_dog->get_all_adv_hit();
		$data['most'] = $this->m_dog->get_all_most_adv();
		$this->load->view('welcome', $data);
	}
}
