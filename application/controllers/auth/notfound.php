<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Notfound extends CI_Controller {

	function __construct() {
			// Call the Model constructor
			parent::__construct();
	}

  public function index($value='')
  {
    $data['message'] = "The page you requested is not found";
    $data['heading'] = "back to <a href='".base_url()."'>homepage</a>";
    $this->load->view('errors/html/error_404', $data);
  }
}
?>
