<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin extends CI_Controller {

	function __construct() {
			// Call the Model constructor
			parent::__construct();
			$this->load->model('m_user');
      $this->load->model('m_masters');
      $this->load->model('m_dog');
	}
	public function check_session($value='')
	{
		$id = $this->session->userdata('admin_sipar')['id_admin'];
		if($id == ''){
			redirect('login/adminlogin');
		}
	}
  public function index($value='')
  {
		$this->check_session();
		$data['iklan'] = $this->m_dog->get_avd_laporan();
		$data['user'] = $this->m_user->get_all_user();
		//print_r($data);die();
    $this->load->view('auth/admin/adminpage', $data);
    //$this->adminunverifiediklan();
  }

	public function get_laporan($value='')
	{
		$data['users'] = $this->m_user->get_all_user_nofilter();
		$data['iklan'] = $this->m_dog->get_all_adv_nofilter();
		$data['iklan_jual'] = $this->m_dog->get_all_adv_nofilter_jual();
		header('Content-Type: application/json');
	 	echo json_encode($data);
	}

  public function profile()
  {
	$this->check_session();
    $userid = $this->session->userdata('user_sipar')['id_user'];
    $data['iklan'] = $this->m_dog->get_adv_per_user($userid);
    $data['jlh_iklan'] = $this->m_dog->get_jumlah_iklan($userid)['jlh_iklan'];
    //echo ($data['iklan'][0]['deskripsi']);die();
    $this->load->view('auth/user/userprofile', $data);
  }

  public function checkoldpassword()
  {
    $pass = $_POST['pass'];
    $userid = $this->session->userdata('user_sipar')['id_user'];
    $res = $this->m_user->checkoldpassword(sha1($pass), $userid);
    if($res){
      $result['pass'] = "match";
    }else{
      $result['pass'] = "diff";
    }

    header('Content-Type: application/json');
	 	echo json_encode($result);
  }

  public function ubahpassword($value='')
  {
    # code...
  }

  public function ubahprofile($value='')
  {
    # code...
  }

	public function adminverifiediklan($sort='')
	{
		$this->check_session();
		//$userid = $this->session->userdata('user_sipar')['id_user'];
		$data['iklan'] = $this->m_dog->get_all_verified_adv($sort);
		$this->load->view('auth/admin/adminiklan', $data);
	}

	public function adminunverifiediklan()
	{
		$this->check_session();
		//$userid = $this->session->userdata('user_sipar')['id_user'];
		$data['iklan'] = $this->m_dog->get_all_unverified_adv();
		$this->load->view('auth/admin/adminiklanunverified', $data);
	}

	public function adminuser($value='')
	{
		$this->check_session();
		$data['users'] = $this->m_user->get_all_user();
		$this->load->view('auth/admin/adminuser', $data);
	}

	public function adminsettings($value='')
	{
		$this->check_session();
		$this->load->view('auth/admin/adminsettings');
	}

	public function lihatiklan($id_iklan='')
	{
		$data['detail'] = $this->m_dog->get_detail_iklan($id_iklan);
		$this->load->view('auth/admin/lihatiklan', $data);
	}

	public function verifikasi($id_iklan='')
	{
		$data['detail'] = $this->m_dog->get_detail_iklan($id_iklan);
		$this->load->view('auth/admin/verifikasi', $data);
	}

	public function verifikasiiklan($id)
	{
		$res = $this->m_dog->verifikasiiklan($id);
		if($res){
				$data['status'] = "success";
		}else {
				$data['status'] = "failed";
		}

		header('Content-Type: application/json');
		echo json_encode($data);

	}

	public function abaikaniklan($id)
	{
		$res = $this->m_dog->abaikaniklan($id);
		if($res){
				$data['status'] = "success";
		}else {
				$data['status'] = "failed";
		}

		header('Content-Type: application/json');
		echo json_encode($data);

	}
}
?>
