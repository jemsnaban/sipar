<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User extends CI_Controller {

	function __construct() {
			// Call the Model constructor
			parent::__construct();
			$this->load->model('m_user');
      $this->load->model('m_masters');
      $this->load->model('m_dog');
	}

	public function check_session($value='')
	{
			$id = $this->session->userdata('user_sipar')['id_user'];
			if($id == ''){
					redirect('home');
			}
	}

  public function index($value='')
  {
    redirect('notfound');
  }

  public function profile()
  {
		$this->check_session();
    $userid = $this->session->userdata('user_sipar')['id_user'];
    $data['iklan'] = $this->m_dog->get_verified_adv_per_user($userid);
    $data['jlh_iklan'] = $this->m_dog->get_jumlah_iklan($userid)['jlh_iklan'];
    //echo ($data['iklan'][0]['deskripsi']);die();
    $this->load->view('auth/user/userprofile', $data);
  }

  public function checkoldpassword()
  {
    $pass = $_POST['pass'];
    $userid = $this->session->userdata('user_sipar')['id_user'];
    $res = $this->m_user->checkoldpassword(sha1($pass), $userid);
    if($res){
      $result['pass'] = "match";
    }else{
      $result['pass'] = "diff";
    }

    header('Content-Type: application/json');
	 	echo json_encode($result);
  }

  public function ubahpassword($value='')
  {
    # code...
  }

  public function ubahprofile($value='')
  {
    # code...
  }

	public function useriklan()
	{
		$userid = $this->session->userdata('user_sipar')['id_user'];
		$data['iklan'] = $this->m_dog->get_unverified_adv_per_user($userid);
		$this->load->view('auth/user/useriklan', $data);
	}

	public function usersettings($value='')
	{
		$this->load->view('auth/user/usersettings');
	}

	public function get_all_most_adv_per_user($value='')
	{
		$userid = $this->session->userdata('user_sipar')['id_user'];
		$data = $this->m_dog->get_all_most_adv_per_user($userid);

		header('Content-Type: application/json');
	 	echo json_encode($data);
	}

	public function get_adv_statistic($value='')
	{
		$userid = $this->session->userdata('user_sipar')['id_user'];
		$data = $this->m_dog->get_adv_statistic($userid);

		header('Content-Type: application/json');
	 	echo json_encode($data);
	}

	public function ubahstatusiklan($id)
	{
		$date = date("Y-m-d");
		$res = $this->m_dog->ubahstatusiklan($id, $date);
		if($res){
				$data['status'] = "success";
				$data['message'] = $res;
		}else {
				$data['status'] = "failed";
				$data['message'] = $res;
		}
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function lihatiklan($id_iklan='')
	{
		$data['detail'] = $this->m_dog->get_detail_iklan($id_iklan);
		$this->load->view('auth/user/lihatiklan_user', $data);
	}
}
?>
