<?php
//CRUD Master, (admin)
class m_masters extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    //load semua jenis anjing, beserta jumlah
    public function get_all_jenis()
    {
      $query = $this->db->query('SELECT a.id_jenis, a.jenis_anjing , count(b.id_jenis) as jlh
                    FROM jenis a left join iklan b on a.id_jenis = b.id_jenis
                    and b.verified = 1 and b.status_jual = 0 GROUP by a.id_jenis');
      if($query){
        return $query->result_array();
      }else{
        return array();
      }
    }
    public function get_all_lokasi()
    {
      $query = $this->db->get('lokasi');
      if($query){
        return $query->result_array();
      }else{
        return array();
      }
    }
}
?>
