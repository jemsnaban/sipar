<?php
//CRUD Iklan
class m_dog extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function get_all_adv_nofilter()
    {
          $q = "SELECT
              SUM(IF(substr(a.tanggal_iklan, 6, 2)='01',1,0))'01',
              SUM(IF(substr(a.tanggal_iklan, 6, 2)='02',1,0))'02',
              SUM(IF(substr(a.tanggal_iklan, 6, 2)='03',1,0))'03',
              SUM(IF(substr(a.tanggal_iklan, 6, 2)='04',1,0))'04',
              SUM(IF(substr(a.tanggal_iklan, 6, 2)='05',1,0))'05',
              SUM(IF(substr(a.tanggal_iklan, 6, 2)='06',1,0))'06',
              SUM(IF(substr(a.tanggal_iklan, 6, 2)='07',1,0))'07',
              SUM(IF(substr(a.tanggal_iklan, 6, 2)='08',1,0))'08',
              SUM(IF(substr(a.tanggal_iklan, 6, 2)='09',1,0))'09',
              SUM(IF(substr(a.tanggal_iklan, 6, 2)='10',1,0))'10',
              SUM(IF(substr(a.tanggal_iklan, 6, 2)='11',1,0))'11',
              SUM(IF(substr(a.tanggal_iklan, 6, 2)='12',1,0))'12'
              from iklan a where a.verified = 1";
          $query = $this->db->query($q);
          if ($query->num_rows() > 0) {
            return $query->row_array();
          }else{
            return array();
          }
    }

    public function get_all_adv_nofilter_jual()
    {
          $q = "SELECT
              IFNULL(SUM(IF(substr(a.tanggal_jual, 6, 2)='01',1,0)),0)'01',
              IFNULL(SUM(IF(substr(a.tanggal_jual, 6, 2)='02',1,0)),0)'02',
              IFNULL(SUM(IF(substr(a.tanggal_jual, 6, 2)='03',1,0)),0)'03',
              IFNULL(SUM(IF(substr(a.tanggal_jual, 6, 2)='04',1,0)),0)'04',
              IFNULL(SUM(IF(substr(a.tanggal_jual, 6, 2)='05',1,0)),0)'05',
              IFNULL(SUM(IF(substr(a.tanggal_jual, 6, 2)='06',1,0)),0)'06',
              IFNULL(SUM(IF(substr(a.tanggal_jual, 6, 2)='07',1,0)),0)'07',
              IFNULL(SUM(IF(substr(a.tanggal_jual, 6, 2)='08',1,0)),0)'08',
              IFNULL(SUM(IF(substr(a.tanggal_jual, 6, 2)='09',1,0)),0)'09',
              IFNULL(SUM(IF(substr(a.tanggal_jual, 6, 2)='10',1,0)),0)'10',
              IFNULL(SUM(IF(substr(a.tanggal_jual, 6, 2)='11',1,0)),0)'11',
              IFNULL(SUM(IF(substr(a.tanggal_jual, 6, 2)='12',1,0)),0)'12'
              from iklan a where a.verified = 1 and a.status_jual = 1";
          $query = $this->db->query($q);
          if ($query->num_rows() > 0) {
            return $query->row_array();
          }else{
            return array();
          }
    }

    public function get_avd_laporan()
    {
      $q = "SELECT
          IFNULL(SUM(IF(a.status_jual=0,1,0)),0)'verified',
          IFNULL(SUM(IF(a.status_jual=1,1,0)),0)'terjual'
          from iklan a where a.verified = 1";
      $query = $this->db->query($q);
      if ($query->num_rows() > 0) {
        return $query->row_array();
      }else{
        return array();
      }
    }

    //load semua iklan anjing
    public function get_all_adv($sort = '')
    {
      switch ($sort) {
        case 1: //hit
          $query = $this->db->query('SELECT a.*, b.jenis_anjing,c.nama_lokasi,x.nama_gambar from iklan a left join jenis b on a.id_jenis = b.id_jenis
                    left join lokasi c on a.id_lokasi = c.id_lokasi
                    left join gambar_iklan x on x.id_iklan = a.id_iklan where a.verified = 1 and  a.status_jual = 0 GROUP BY a.id_iklan order by a.hit desc');
          break;
        case 2: // nama asc
          $query = $this->db->query('SELECT a.*, b.jenis_anjing,c.nama_lokasi,x.nama_gambar from iklan a left join jenis b on a.id_jenis = b.id_jenis
                    left join lokasi c on a.id_lokasi = c.id_lokasi
                    left join gambar_iklan x on x.id_iklan = a.id_iklan where a.verified = 1 and  a.status_jual = 0 GROUP BY a.id_iklan order by a.judul_iklan asc');
          break;
        case 3: //nama desc
          $query = $this->db->query('SELECT a.*, b.jenis_anjing,c.nama_lokasi,x.nama_gambar from iklan a left join jenis b on a.id_jenis = b.id_jenis
                    left join lokasi c on a.id_lokasi = c.id_lokasi
                    left join gambar_iklan x on x.id_iklan = a.id_iklan where a.verified = 1 and  a.status_jual = 0 GROUP BY a.id_iklan order by a.judul_iklan desc');
          break;
        case 4: //harga asc
        $query = $this->db->query('SELECT a.*, b.jenis_anjing,c.nama_lokasi,x.nama_gambar from iklan a left join jenis b on a.id_jenis = b.id_jenis
                  left join lokasi c on a.id_lokasi = c.id_lokasi
                  left join gambar_iklan x on x.id_iklan = a.id_iklan where a.verified = 1 and  a.status_jual = 0 GROUP BY a.id_iklan order by a.harga asc');
        break;
        case 5: //harga desc
        $query = $this->db->query('SELECT a.*, b.jenis_anjing,c.nama_lokasi,x.nama_gambar from iklan a left join jenis b on a.id_jenis = b.id_jenis
                  left join lokasi c on a.id_lokasi = c.id_lokasi
                  left join gambar_iklan x on x.id_iklan = a.id_iklan where a.verified = 1 and  a.status_jual = 0 GROUP BY a.id_iklan order by a.harga desc');
        break;
        default:
          $query = $this->db->query('SELECT a.*, b.jenis_anjing,c.nama_lokasi,x.nama_gambar from iklan a left join jenis b on a.id_jenis = b.id_jenis
                    left join lokasi c on a.id_lokasi = c.id_lokasi
                    left join gambar_iklan x on x.id_iklan = a.id_iklan where a.verified = 1 and  a.status_jual = 0 GROUP BY a.id_iklan');
          break;
      }
      //$query = $this->db->query('SELECT *, b.nama_gambar from iklan a left join gambar_iklan b on a.id_iklan = b.id_iklan GROUP BY a.id_iklan');
      if ($query->num_rows() > 0) {
        return $query->result_array();
      }else{
        return array();
      }
    }

    public function get_all_adv_hit()
    {
      $query = $this->db->query('SELECT sum(a.hit) as hit, b.jenis_anjing, a.id_jenis 
        FROM iklan a left join jenis b on a.id_jenis = b.id_jenis where a.verified = 1 
        and a.status_jual = 0
        group by a.id_jenis order by hit desc LIMIT 3');
      if ($query->num_rows() > 0) {
        return $query->result_array();
      }else{
        return array();
      }
    }

    public function get_all_verified_adv($value='')
    {
      if ($value == 1){
        $query = $this->db->query('SELECT a.*, b.jenis_anjing,c.nama_lokasi,x.nama_gambar from
                iklan a left join jenis b on a.id_jenis = b.id_jenis
                left join lokasi c on a.id_lokasi = c.id_lokasi
                left join gambar_iklan x on x.id_iklan = a.id_iklan where a.verified = 1 and a.status_jual = 1 GROUP BY a.id_iklan');
      }else {
        $query = $this->db->query('SELECT a.*, b.jenis_anjing,c.nama_lokasi,x.nama_gambar from
                iklan a left join jenis b on a.id_jenis = b.id_jenis
                left join lokasi c on a.id_lokasi = c.id_lokasi
                left join gambar_iklan x on x.id_iklan = a.id_iklan where a.verified = 1 GROUP BY a.id_iklan');
      }
      if ($query->num_rows() > 0) {
        return $query->result_array();
      }else{
        return array();
      }
    }

    public function get_all_unverified_adv($value='')
    {
      $query = $this->db->query('SELECT a.*, b.jenis_anjing,c.nama_lokasi,x.nama_gambar
                from iklan a left join jenis b on a.id_jenis = b.id_jenis
                left join lokasi c on a.id_lokasi = c.id_lokasi
                left join gambar_iklan x on x.id_iklan = a.id_iklan where a.verified = 0 GROUP BY a.id_iklan');
      if ($query->num_rows() > 0) {
        return $query->result_array();
      }else{
        return array();
      }
    }

    public function get_all_most_adv()
    {
      $query = $this->db->query('SELECT count(a.id_jenis) as jlh, b.jenis_anjing,
              a.id_jenis FROM iklan a left join jenis b on a.id_jenis = b.id_jenis 
              where a.verified = 1 and a.status_jual = 0 group by a.id_jenis order by jlh desc LIMIT 3');
      if ($query->num_rows() > 0) {
        return $query->result_array();
      }else{
        return array();
      }
    }

    public function get_all_most_adv_per_user($iduser='')
    {
      //echo $iduser;
      $query = $this->db->query("SELECT count(a.id_jenis) as jlh, b.jenis_anjing,
              a.id_jenis FROM iklan a left join jenis b on a.id_jenis = b.id_jenis 
              where a.id_user = $iduser group by a.id_jenis order by jlh desc");
      if ($query->num_rows() > 0) {
        return $query->result_array();
      }else{
        return array();
      }
    }

    public function get_adv_statistic($userid='')
    {
      if($userid == ''){
        $q = "SELECT a.judul_iklan, sum(if(a.tanggal_iklan != '', 1, 0)) 'terpasang', sum(if(a.status_jual = 1, 1, 0)) 'terjual' from iklan a GROUP by a.id_jenis";
      }else{
        $q = "SELECT a.judul_iklan, sum(if(a.tanggal_iklan != '', 1, 0)) 'terpasang', sum(if(a.status_jual = 1, 1, 0)) 'terjual' from iklan a where a.id_user = $userid GROUP by a.id_jenis";
      }
      $query = $this->db->query($q);
      if ($query->num_rows() > 0) {
        return $query->result_array();
      }else{
        return array();
      }
    }

    public function get_max_id_iklan()
    {
      $query = $this->db->query("select max(id_iklan) as id_iklan from iklan");
      if ($query->num_rows() > 0) {
        return $query->row_array();
      }else{
        return false;
      }
    }

    public function insert_adv_image($id, $name)
    {
      $data = array('id_iklan' => $id, 'nama_gambar' => $name );
      $query = $this->db->insert('gambar_iklan', $data);
      if ($query) {
        return $this->db->insert_id();
      }else{
        return false;
      }
    }

    public function uploadiklan($data)
    {
      $query = $this->db->insert('iklan', $data);
      if ($query) {
        return $this->db->insert_id();
      }else{
        return false;
      }
    }

    public function get_unverified_adv_per_user($iduser)
    {
      $query = "SELECT a.*, b.jenis_anjing,c.nama_lokasi,x.nama_gambar from iklan a left join jenis b on a.id_jenis = b.id_jenis
                left join lokasi c on a.id_lokasi = c.id_lokasi
                left join gambar_iklan x on x.id_iklan = a.id_iklan
                where a.id_user = $iduser and a.verified = 0 group by a.id_iklan";
      $result = $this->db->query($query);
      if ($result) {
        return $result->result_array();
      }else{
        return array();
      }
    }

    public function get_verified_adv_per_user($iduser)
    {
      $query = "SELECT a.*, b.jenis_anjing,c.nama_lokasi,x.nama_gambar from iklan a left join jenis b on a.id_jenis = b.id_jenis
                left join lokasi c on a.id_lokasi = c.id_lokasi
                left join gambar_iklan x on x.id_iklan = a.id_iklan
                where a.id_user = $iduser and a.verified = 1 group by a.id_iklan";
      $result = $this->db->query($query);
      if ($result) {
        return $result->result_array();
      }else{
        return array();
      }
    }

    public function get_jumlah_iklan($id)
    {
      $query = $this->db->query("SELECT count(id_iklan) as jlh_iklan from iklan where id_user = $id");
      if($query->num_rows() > 0){
        return $query->row_array();
      }else{
        return array('jlh_iklan' => 0);
      }
    }

    public function get_detail_iklan($id_iklan)
    {
      $q = "SELECT a.*, b.first_name,b.last_name, b.date_join,b.no_hp, c.jenis_anjing, d.nama_lokasi,x.nama_gambar
      from iklan a left join gambar_iklan x on a.id_iklan = x.id_iklan left join user b on a.id_user = b.id_user LEFT JOIN jenis c on c.id_jenis = a.id_jenis
      LEFT join lokasi d on d.id_lokasi = a.id_lokasi WHERE a.id_iklan = $id_iklan";
      $query = $this->db->query($q);
      if($query->num_rows() > 0){
        //update hit
        $q = "Update iklan set hit = hit+1 where id_iklan = $id_iklan";
        $r = $this->db->query($q);
        if($r){
            return $query->result_array();
        }else{
            return array();
        }
      }else{
        return array();
      }
    }

    public function get_adv_per_jenis($id_jenis)
    {
      $q = "SELECT a.*, b.first_name,b.last_name, b.date_join, c.jenis_anjing, d.nama_lokasi, x.nama_gambar
      from iklan a left join gambar_iklan x on x.id_iklan = a.id_iklan left join user b on a.id_user = b.id_user
      LEFT JOIN jenis c on c.id_jenis = a.id_jenis LEFT join lokasi d on d.id_lokasi = a.id_lokasi WHERE a.id_jenis = $id_jenis and a.verified = 1 and  a.status_jual = 0 GROUP by a.id_iklan";
      $query = $this->db->query($q);
      if($query->num_rows() > 0){
        return $query->result_array();
      }else{
        return array();
      }
    }

    public function verifikasiiklan($id)
    {
      $q = "update iklan set verified = 1 where id_iklan = $id";
      $r = $this->db->query($q);
      if($r){
          return true;
      }else{
          return false;
      }
    }

    public function abaikaniklan($id)
    {
      $q = "update iklan set verified = 2 where id_iklan = $id";
      $r = $this->db->query($q);
      if($r){
          return true;
      }else{
          return false;
      }
    }

    public function ubahstatusiklan($id, $date)
    {
      $q = "update iklan set status_jual = 1, tanggal_jual = '$date' where id_iklan = $id";
      $r = $this->db->query($q);
      if($r){
          return true;
      }else{
          return false;
      }
    }

}
?>
