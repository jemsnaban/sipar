<?php
//CRUD USER
class m_user extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function authenticateuser($username, $password)
    {
      $query = $this->db->get_where('user', array('username' => $username, 'password' => $password, 'role' => 'user'));
      if($query){
        return $query->row_array();
      }else{
        return false;
      }
    }

    public function authenticateadmin($username, $password)
    {
      $query = $this->db->get_where('user', array('username' => $username, 'password' => $password, 'role' => 'admin'));
      if($query){
        return $query->row_array();
      }else{
        return false;
      }
    }

    public function registeruser($data)
    {
      $query = $this->db->insert('user', $data);
      if ($query) {
        return $this->db->insert_id();
      }else{
        return false;
      }
    }

    public function checkoldpassword($oldpass, $id)
    {
      $query = $this->db->get_where('user', array('id_user' => $id, 'password' => $oldpass));
      if($query->num_rows() > 0){
        return true;
      }else{
        return false;
      }
    }

    public function get_all_user($value='')
    {
      $q = "SELECT a.*, count(b.id_iklan) as jlh_iklan from user a left join iklan b on a.id_user = b.id_user where a.role like 'user' group by a.id_user";
      $query = $this->db->query($q);
      if($query){
        return $query->result_array();
      }else{
        return array();
      }
    }

    public function get_all_user_nofilter($value='')
    {
      $q = "SELECT
              SUM(IF(substr(a.date_join, 6, 2)='01',1,0))'01',
              SUM(IF(substr(a.date_join, 6, 2)='02',1,0))'02',
              SUM(IF(substr(a.date_join, 6, 2)='03',1,0))'03',
              SUM(IF(substr(a.date_join, 6, 2)='04',1,0))'04',
              SUM(IF(substr(a.date_join, 6, 2)='05',1,0))'05',
              SUM(IF(substr(a.date_join, 6, 2)='06',1,0))'06',
              SUM(IF(substr(a.date_join, 6, 2)='07',1,0))'07',
              SUM(IF(substr(a.date_join, 6, 2)='08',1,0))'08',
              SUM(IF(substr(a.date_join, 6, 2)='09',1,0))'09',
              SUM(IF(substr(a.date_join, 6, 2)='10',1,0))'10',
              SUM(IF(substr(a.date_join, 6, 2)='11',1,0))'11',
              SUM(IF(substr(a.date_join, 6, 2)='12',1,0))'12'
              from user a where a.role = 'user'";
      $query = $this->db->query($q);
      if($query){
        return $query->row_array();
      }else{
        return array();
      }
    }
}
?>
