<?php
  //var_dump($this->session->flashdata('message'));
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Home - Jualanjing</title>
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/bootstrap/css/bootstrap.min.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/styles/custom.css" media="screen" title="no title" charset="utf-8">
    <link href="<?php echo base_url() ?>/assets/styles/magnific-popup.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>/assets/styles/etalage.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>/assets/plugins/datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
  </head>
  <body>
    <div class=" top-nav">
      <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url() ?>">TokoAnjing</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li><a href="<?php echo base_url() ?>">Home <span class="sr-only">(current)</span></a></li>
              <li class="active"><a href="<?php echo base_url() ?>home/pasangiklan">Pasang Iklan</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown active">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Welcome, <?php echo $this->session->userdata('user_sipar')['username'] ?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo base_url() ?>user/profile">Lihat Profile</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="<?php echo base_url() ?>login/logout">Keluar</a></li>
                </ul>
              </li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>

    </div>
    <!--container utama -->
    <div class="container main-container">
      <div class="row">
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url() ?>">Home</a></li>
          <li class="active">Pasang Iklan</li>
        </ol>
        <hr>
        <div class="dog-inner">
          <div class="col-md-8 col-md-offset-2">
            <?php if ($this->session->flashdata('message') != NULL): ?>
              <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $this->session->flashdata('message'); ?>
              </div>
            <?php endif; ?>
            <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo base_url() ?>home/uploadiklan">
              <div class="form-group">
                <label for="juduliklan" class="col-sm-2 control-label">Judul Iklan</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="juduliklan" id="juduliklan" placeholder="Judul Iklan (maksimal 60 karakter)" required="" maxlength="60">
                </div>
              </div>
              <div class="form-group">
                <label for="jenisanjing" class="col-sm-2 control-label">Jenis Anjing</label>
                <div class="col-sm-10">
                  <select class="form-control" name="jenis_anjing">
                    <?php
                      foreach ($jenis_anjing as $key) {
                        echo '<option value="'.$key['id_jenis'].'">'.$key['jenis_anjing'].'</option>';
                      }
                    ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="lokasi" class="col-sm-2 control-label">Lokasi</label>
                <div class="col-sm-10">
                  <select class="form-control" name="lokasi">
                    <?php
                      foreach ($lokasi as $key) {
                        echo '<option value="'.$key['id_lokasi'].'">'.$key['nama_lokasi'].'</option>';
                      }
                    ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="harga" class="col-sm-2 control-label">Tanggal lahir</label>
                <div class="col-sm-10">
                  <div class="input-group date">
                    <input type="text" id="datepicker" name="tanggal_lahir" class="form-control" value="<?php echo date('d-m-Y'); ?>">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
                </div>
              </div>
              <div class="form-group">
                <label for="harga" class="col-sm-2 control-label">Harga</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="harga" id="harga" placeholder="Harga" required="" min="1">
                </div>
              </div>
              <div class="form-group">
                <label for="lokasi" class="col-sm-2 control-label">Deskripsi</label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="deskripsi" rows="8" cols="40" style="width:100%" id="control-area"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label for="imgInp" class="col-sm-2 control-label">Gambar Verifikasi (stambum)</label>
                <div class="col-sm-10">
                  <input type="file" id="imgInp" name="avatar" accept="image/*" required="">
                  <!--img id="blah" src="<?php echo base_url() ?>assets/images/grey.jpg" alt="your image" width="50%" style="margin-top:10px;"/-->
                </div>
              </div>
              <div class="form-group">
                <label for="imgInp" class="col-sm-2 control-label">Gambar Tambahan (bisa lebih dari 1) </label>
                <div class="col-sm-10">
                  <p class="red">
                    * minimal 1 file (bisa lebih dari 1 file)
                  </p>
                  <input name="userfile[]" id="userfile" type="file" multiple="" accept="image/*" required="">
                </div>
              </div>
              <div class="form-group">
                <label for="vidInput" class="col-sm-2 control-label">Video Anjing</label>
                <div class="col-sm-10">
                  <p class="red">
                    * tipe .3gp, ukuran maksimal 6 mb
                  </p>
                  <p>
                    * kunjungi link <a href="http://video.online-convert.com/convert-to-3gp" target="_blank">ini</a> untuk convert video menjadi .3gp
                  </p>
                  <div id="message"></div>
                  <input type="file" accept="video/*" id="vidInput" name="video" required=""/>
                  <!--video controls autoplay width="50%"></video-->
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-primary">Upload Iklan</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!--akhir container utama -->

    <footer>
      <div class="container">
        <div class="row">
          <center>Copyright @SIPAR 2016 </center>
        </div>
      </div>
    </footer>
    <script src="<?php echo base_url() ?>/assets/js/jQuery-2.2.0.min.js" charset="utf-8"></script>
    <script src="<?php echo base_url() ?>/assets/bootstrap/js/bootstrap.min.js" charset="utf-8"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.magnific-popup.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.etalage.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/plugins/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script>
				jQuery(document).ready(function($){

          $('#datepicker').datepicker({
            format: 'dd-mm-yyyy'
          });

					$('#etalage').etalage({
						thumb_image_width: 300,
						thumb_image_height: 400,
						show_hint: true,
						click_callback: function(image_anchor, instance_id){
							alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
						}
					});

          $('#harga').on('change', function() {
              var hrg = $(this).val();
              if(Number(hrg) < 1){
                  $(this).val(1);
              }
          })

          //ckeditor
        //  CKEDITOR.replace('deskripsi');

          function readURL(input) {
              if (input.files && input.files[0]) {
                  var reader = new FileReader();

                  reader.onload = function (e) {
                      $('#blah').attr('src', e.target.result);
                  }

                  reader.readAsDataURL(input.files[0]);
              }
          }

          $("#imgInp").change(function(){
              //readURL(this);
          });

          //localFileVideoPlayer();

          function localFileVideoPlayer() {

            var URL = window.URL || window.webkitURL;
            var displayMessage = function (message, isError) {
              var element = document.querySelector('#message')
              element.innerHTML = message
              element.className = isError ? 'error' : 'info'
            }
            var playSelectedFile = function () {
              //alert("fuck");
              var file = this.files[0]
              var type = file.type
              var videoNode = document.querySelector('video')
              var canPlay = videoNode.canPlayType(type)
              if (canPlay === '') canPlay = 'no'
              var message = 'Can play type "' + type + '": ' + canPlay
              var isError = canPlay === 'no'
              displayMessage(message, isError)

              if (isError) {
                return
              }

              var fileURL = URL.createObjectURL(file)
              videoNode.src = fileURL
            }
            var inputNode = document.querySelector('#vidInput');
            inputNode.addEventListener('change', playSelectedFile, false);
          }
			});

		</script>
  </body>
</html>
