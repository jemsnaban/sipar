<?php
  //var_dump($this->session->userdata('user_sipar'));
  if ($this->session->userdata('user_sipar') != NULL) {
    $user = $this->session->userdata('user_sipar');
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Home - Jualanjing</title>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/styles/custom.css" media="screen" title="no title" charset="utf-8">
  </head>
  <body>
    <div class=" top-nav">
      <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url() ?>">TokoAnjing</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="active"><a href="<?php echo base_url() ?>">Home <span class="sr-only">(current)</span></a></li>
              <li><a href="<?php echo base_url() ?>home/pasangiklan">Pasang Iklan</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <!-- Cek User sudah Login atau tidak -->
              <?php if ($this->session->userdata('user_sipar') != NULL): ?>
                <li class="dropdown active">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Welcome, <?php echo $this->session->userdata('user_sipar')['username'] ?> <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo base_url() ?>user/profile">Lihat Profile</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="<?php echo base_url() ?>login/logout">Keluar</a></li>
                  </ul>
                </li>
              <?php else: ?>
                <li class="active"><a href="<?php echo base_url() ?>login">&laquo; Masuk &raquo;</a></li>
              <?php endif; ?>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>

    </div>
    <?php if ($carousel == "true"): ?>
      <div class="container carousel-container">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img src="<?php echo base_url() ?>/assets/images/carousel-1.jpg" alt="carousel 1">
              <div class="carousel-caption">
                <h4>Siberian Husky</h4>
              </div>
            </div>
            <div class="item">
              <img src="<?php echo base_url() ?>/assets/images/carousel-2.jpg" alt="carousel 2">
              <div class="carousel-caption">
                <h4>Siberian Husky</h4>
              </div>
            </div>
            <div class="item">
              <img src="<?php echo base_url() ?>/assets/images/carousel-3.jpg" alt="carousel 3">
              <div class="carousel-caption">
                <h4>Siberian Husky</h4>
              </div>
            </div>
          </div>

          <!-- Controls -->
          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    <?php endif; ?>
    <!--container utama -->
    <div class="container main-container" id="main">
      <div class="row">
        <ol class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li class="active">Iklan</li>
        </ol>
        <hr>
        <div class="dog-inner">
          <div class="col-md-3 col-sm-4 col-xs-12">
            <h5>Jenis Anjing</h5>
            <ul class="list-group">
              <?php
                foreach ($jenis_anjing as $key) {
                  echo '<li class="list-group-item">
                    <span class="badge">'.$key['jlh'].'</span>
                    <a href="'.base_url().'home/jenis/'.$key['id_jenis'].'">'.$key['jenis_anjing'].'</a>
                  </li>';
                }
              ?>
            </ul>
          </div>
          <div class="col-md-7 col-sm-6 col-xs-12">
            <div class="row" style="margin-bottom:20px;">
              <div class="col-md-3 col-sm-5 col-xs-3 right">
                <input type="hidden" id="idSort" value="<?php echo isset($sort) ? $sort : 0 ?>"/>
                <select class="form-control" name="lokasi" id="lokasi">
                  <option value="l" selected="">Terpopuler</option>
                  <option value="2">Nama (A - Z)</option>
                  <option value="3">Nama (Z - A)</option>
                  <option value="4">Harga (low - high)</option>
                  <option value="5">Harga (high - low)</option>
                </select>
              </div>
            </div>
            <div class="row">
              <?php
                if(isset($iklan) && !empty($iklan)){
                  foreach ($iklan as $key) {
                    echo '<div class="col-sm-6 col-md-4 col-xs-6 thumbnail-div">
                      <div class="thumbnail">
                        <img src="'.base_url().'uploads/images/'.$key['nama_gambar'].'" alt="">
                        <div class="caption">
                          <p style="margin-bottom:0px;">
                            <strong><a href="'.base_url().'home/detail/'.$key['id_iklan'].'">'.$key['judul_iklan'].'</a></strong>
                          </p>
                          <p style="margin-bottom:0px;">IDR '.$key['harga'].'</p>
                        </div>
                      </div>
                    </div>';
                  }
                }
              ?>
            </div>
          </div>
          <div class="col-md-2 col-sm-2 col-xs12">
            <div class="row" style="margin-bottom:20px;">
              <div class="col-md-3 col-sm-5 col-xs-3 right">
                <br><br>
              </div>
            </div>
            <div class="row">
              <h5><b>Most Popular Views</b></h5>
              <div class="rightsidebar">
                <?php
                    if(!empty($hit)){
                        foreach ($hit as $key) {
                            echo '<p><a href="'.base_url().'home/jenis/'.$key['id_jenis'].'">'.$key['jenis_anjing'].'</a> : ';
                            echo $key['hit'] . '</p>';
                        }
                    }
                ?>
              </div>
              <br>
              <h5><b>Most Advertised</b></h5>
              <div class="rightsidebar">
                <?php
                    if(!empty($most)){
                        foreach ($most as $key) {
                            echo '<p><a href="'.base_url().'home/jenis/'.$key['id_jenis'].'">'.$key['jenis_anjing'].'</a> : ';
                            echo $key['jlh'] . '</p>';
                        }
                    }
                ?>
              </div>
              <br>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--akhir container utama -->

    <footer>
      <div class="container">
        <div class="row">
          <center>Copyright @SIPAR 2016 </center>
        </div>
      </div>
    </footer>
    <script src="<?php echo base_url() ?>assets/js/jQuery-2.2.0.min.js" charset="utf-8"></script>
    <script src="<?php echo base_url() ?>assets/bootstrap/js/bootstrap.min.js" charset="utf-8"></script>
    <script type="text/javascript">
      $(document).ready(function () {
          $('#lokasi').on('change', function () {
              var d = $(this).val();
              window.location.href = "<?php echo base_url() ?>home/sort/"+d;
          })

          //sorting
          var idSort = $('#idSort').val();
          if(Number(idSort) > 0){
              $('select[name="lokasi"]').val(idSort);
          }
      });
    </script>
  </body>
</html>
