<?php
  //var_dump($this->session->flashdata('message'));
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Home - Jualanjing</title>
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/bootstrap/css/bootstrap.min.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/styles/custom.css" media="screen" title="no title" charset="utf-8">
    <link href="<?php echo base_url() ?>/assets/styles/magnific-popup.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/plugins/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>/assets/styles/etalage.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>/assets/plugins/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
  </head>
  <body>
    <div class=" top-nav">
      <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url() ?>admin">TokoAnjing</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li><a href="<?php echo base_url() ?>admin">Home <span class="sr-only">(current)</span></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown active">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Howdy, <?php echo $this->session->userdata('admin_sipar')['username'] ?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo base_url() ?>login/logoutadmin">Keluar</a></li>
                </ul>
              </li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>

    </div>
    <!--container utama -->
    <div class="container main-container">
      <div class="row">
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url() ?>admin">Home</a></li>
          <li class="active">Dashboard</li>
        </ol>
        <hr>
        <div class="dog-inner">
          <div class="col-md-3 col-sm-4">
            <ul class="nav nav-pills nav-stacked">
              <li role="presentation" class="active"><a href="<?php echo base_url() ?>admin/index">Dashboard</a></li>
              <li role="presentation"><a href="<?php echo base_url() ?>admin/adminunverifiediklan">Iklan blm verified</a></li>
              <li role="presentation"><a href="<?php echo base_url() ?>admin/adminverifiediklan">Iklan Aktif</a></li>
              <li role="presentation"><a href="<?php echo base_url() ?>admin/adminuser">User</a></li>
              <li role="presentation"><a href="<?php echo base_url() ?>admin/adminsettings">Pengaturan</a></li>
            </ul>
          </div>
          <div class="col-md-9 col-sm-8">
            <section class="content">
              <!-- Info boxes -->
              <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-aqua">
                      <i class="fa fa-cart-plus" aria-hidden="true"></i>
                    </span>

                    <div class="info-box-content">
                      <span class="info-box-text">Penjualan</span>
                      <span class="info-box-number"><a href="<?php echo base_url() ?>admin/adminverifiediklan/1"><?php echo $iklan['terjual']; ?></a></span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
                <!-- /.col -->

                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-green">
                      <i class="fa fa-sellsy" aria-hidden="true"></i>
                    </span>

                    <div class="info-box-content">
                      <span class="info-box-text">Iklan</span>
                      <span class="info-box-number"><a href="<?php echo base_url() ?>admin/adminverifiediklan"><?php echo ($iklan['terjual'] + $iklan['verified']); ?></a></span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-yellow">
                      <i class="fa fa-user-secret" aria-hidden="true"></i>
                    </span>

                    <div class="info-box-content">
                      <span class="info-box-text">User</span>
                      <span class="info-box-number"><a href="<?php echo base_url() ?>admin/adminuser"><?php echo count($user); ?></a></span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row container-laporan">
                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto">
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
    <!--akhir container utama -->

    <footer>
      <div class="container">
        <div class="row">
          <center>Copyright @SIPAR 2016 </center>
        </div>
      </div>
    </footer>
    <script src="<?php echo base_url() ?>/assets/js/jQuery-2.2.0.min.js" charset="utf-8"></script>
    <script src="<?php echo base_url() ?>/assets/bootstrap/js/bootstrap.min.js" charset="utf-8"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.magnific-popup.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.etalage.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/plugins/ckeditor/ckeditor.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
    <script>
				jQuery(document).ready(function($){
          $('#err_pass').hide();$('#err_old_pass').hide();
          $('#pass').on('change', function () {
            checkPass();
          });
          $('#retype_pass').on('change', function () {
            checkPass();
          });

          function checkPass() {
            var pass = $('#pass').val();
            var retype = $('#retype_pass').val();

            if(pass != retype){
              //alert("no");
              $('#err_pass').show();
            }else{
              //alert("ues");
              $('#err_pass').hide();
            }
          }

          $('#oldpass').on('change', function () {
            cekOldPass();
          });

          function cekOldPass(){
            var arr = $('#oldpass').val();
            //alert($('#oldpass').val());
            $.ajax({
              type : "POST",
              url:'<?php echo base_url() ?>user/checkoldpassword',
              data: {'pass' : arr},
              success: function (data) {
                console.log(data);
                if (data['pass'] == 'diff') {
                  $('#err_old_pass').show();
                }else {
                  $('#err_old_pass').hide();
                }
              },
              error: function (data) {
                console.log(data);
              }
            })
          }

          $.ajax({
              type: "GET",
              url: "<?php echo base_url() ?>admin/get_laporan",
              success: function (data) {
                laporkan(data);
                console.log(data);
              },
              error: function (data) {
                console.log(data);
              }
          })

          function laporkan(data) {
            $('.kontainer-laporan').children().remove();
            $('.kontainer-laporan').append('<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto">');
            var user = data['users'];
            console.log(Object.keys(user).length);
            var iklan = data['iklan'];
            var iklanjual = data['iklan_jual'];
            var u_user = []; var u_iklan = []; var u_iklan_jual = [];
            var cat = ['01', '02', '03', '04', '05', '06',
                '07', '08', '09', '10', '11', '12']
            for (var i = 0 ; i < Object.keys(user).length; i++){
                var c = cat[i];
                //console.log(user[c]);
                u_user.push(Number(user[c]));
            }
            for (var i = 0 ; i < Object.keys(iklanjual).length; i++){
                var c = cat[i];
                //console.log(user[c]);
                u_iklan_jual.push(Number(iklanjual[c]));
            }
            for (var i = 0 ; i < Object.keys(iklan).length; i++){
                var c = cat[i];
                //console.log(user[c]);
                u_iklan.push(Number(iklan[c]));
            }
            console.log(u_user);
            $('#container').highcharts({
                title: {
                    text: 'Perkembangan Iklan Terpasang, Iklan Terjual, dan Jumlah User tahun 2016',
                    x: -20 //center
                },
                xAxis: {
                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                },
                yAxis: {
                    title: {
                        text: 'Jumlah'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: ''
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [{
                    name: 'Jumlah User',
                    data: u_user
                }, {
                    name: 'Iklan Terjual',
                    data: u_iklan_jual
                }, {
                    name: 'Iklan Terpasang',
                    data: u_iklan
                }]
            });
          }
			  });

		</script>
  </body>
</html>
