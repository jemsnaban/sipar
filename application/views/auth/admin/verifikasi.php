<?php
  //echo json_encode($detail);
  $det = $detail[0];
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Home - Jualanjing</title>
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/bootstrap/css/bootstrap.min.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/styles/custom.css" media="screen" title="no title" charset="utf-8">
    <link href="<?php echo base_url() ?>/assets/styles/magnific-popup.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/plugins/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>/assets/styles/etalage.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>/assets/plugins/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
  </head>
  <body>
    <div class=" top-nav">
      <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url() ?>admin">TokoAnjing</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li><a href="<?php echo base_url() ?>admin">Home <span class="sr-only">(current)</span></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown active">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Welcome, <?php echo $this->session->userdata('admin_sipar')['username'] ?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo base_url() ?>login/logout">Keluar</a></li>
                </ul>
              </li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>

    </div>
    <!--container utama -->
    <div class="container main-container">
      <div class="row">
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url() ?>admin">Home</a></li>
          <li class="active">verifikasi</li>
        </ol>
        <hr>
        <div class="dog-inner">
          <div class="col-md-8 col-md-offset-2">
            <?php if ($this->session->flashdata('message') != NULL): ?>
              <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $this->session->flashdata('message'); ?>
              </div>
            <?php endif; ?>
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="verifikasi" action="<?php echo base_url() ?>admin/verifikasiiklan">
              <div class="form-group">
                <label for="juduliklan" class="col-sm-2 control-label">Judul Iklan</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="juduliklan" id="juduliklan" placeholder="Judul Iklan (maksimal 60 karakter)" disabled="" value="<?php echo $det['judul_iklan'] ?>" >
                </div>
              </div>
              <div class="form-group">
                <label for="jenisanjing" class="col-sm-2 control-label">Jenis Anjing</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="jenisanjing" id="jenisanjing" placeholder="" disabled="" value="<?php echo $det['jenis_anjing'] ?>">
                </div>
              </div>
              <div class="form-group">
                <label for="lokasi" class="col-sm-2 control-label">Lokasi</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="lokasi" id="lokasi" placeholder="" disabled="" value="<?php echo $det['nama_lokasi'] ?>">
                </div>
              </div>
              <div class="form-group">
                <label for="lokasi" class="col-sm-2 control-label">Lahir</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="lokasi" id="lokasi" placeholder="" disabled="" value="<?php echo $det['tanggal_lahir'] ?>">
                </div>
              </div>
              <div class="form-group">
                <label for="harga" class="col-sm-2 control-label">Harga</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="harga" id="harga" placeholder="Harga" required="" min="1" value="<?php echo $det['harga'] ?>" disabled="">
                </div>
              </div>
              <div class="form-group">
                <label for="lokasi" class="col-sm-2 control-label">Deskripsi</label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="deskripsi" rows="8" cols="40" style="width:100%" id="control-area" disabled=""><?php echo $det['deskripsi'] ?></textarea>
                </div>
              </div>
              <div class="form-group">
                <label for="imgInp" class="col-sm-2 control-label">Gambar Verifikasi (stambum)</label>
                <div class="col-sm-10">
                  <img style="max-width:500px;" src="<?php echo base_url(); ?>uploads/images/<?php echo $det['gambar']; ?>">
                </div>
              </div>
              <div class="form-group">
                <label for="imgInp" class="col-sm-2 control-label">Gambar Tambahan </label>
                <div class="col-sm-10">
                  <?php foreach ($detail as $key): ?>
                    <div class="col-sm-4"><img width="100%" src="<?php echo base_url() ?>uploads/images/<?php echo $key['nama_gambar']; ?>"></div>
                  <?php endforeach; ?>
                </div>
              </div>
              <div class="form-group">
                <label for="vidInput" class="col-sm-2 control-label">Video Anjing</label>
                <div class="col-sm-10">
                  <video width="320" height="240" controls>
                    <source src="<?php echo base_url(); ?>uploads/videos/<?php echo $det['video']; ?>" type="video/mp4">
                    <source src="<?php echo base_url(); ?>uploads/videos/<?php echo $det['video']; ?>" type="video/ogg">
                    <source src="<?php echo base_url(); ?>uploads/videos/<?php echo $det['video']; ?>" type="video/3gp">
                    <source src="<?php echo base_url(); ?>uploads/videos/<?php echo $det['video']; ?>" type="video/webm">
                  Your browser does not support the video tag.
                  </video>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <input type="hidden" name="id_iklan" id="id_iklan" value="<?php echo $det['id_iklan']; ?>"/>
                  <button type="submit" class="btn btn-primary">Verifikasi Iklan</button>
                  <button type="button" class="btn btn-danger" id="buttoncancel">Cancel</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!--akhir container utama -->

    <footer class="navbar-fixed-bottom">
      <div class="container">
        <div class="row">
          <center>Copyright @SIPAR 2016 </center>
        </div>
      </div>
    </footer>
    <script src="<?php echo base_url() ?>/assets/js/jQuery-2.2.0.min.js" charset="utf-8"></script>
    <script src="<?php echo base_url() ?>/assets/bootstrap/js/bootstrap.min.js" charset="utf-8"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.magnific-popup.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.etalage.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/plugins/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
    <script>
				jQuery(document).ready(function($){

          $('#etalage').etalage({
              thumb_image_width: 300,
              thumb_image_height: 300,

              show_hint: true,
              click_callback: function(image_anchor, instance_id){
                alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
              }
            });

          $('#err_pass').hide();$('#err_old_pass').hide();
          $('#pass').on('change', function () {
            checkPass();
          });
          $('#retype_pass').on('change', function () {
            checkPass();
          });

          function checkPass() {
            var pass = $('#pass').val();
            var retype = $('#retype_pass').val();

            if(pass != retype){
              //alert("no");
              $('#err_pass').show();
            }else{
              //alert("ues");
              $('#err_pass').hide();
            }
          }

          $('#oldpass').on('change', function () {
            cekOldPass();
          });

          function cekOldPass(){
            var arr = $('#oldpass').val();
            //alert($('#oldpass').val());
            $.ajax({
              type : "POST",
              url:'<?php echo base_url() ?>user/checkoldpassword',
              data: {'pass' : arr},
              success: function (data) {
                console.log(data);
                if (data['pass'] == 'diff') {
                  $('#err_old_pass').show();
                }else {
                  $('#err_old_pass').hide();
                }
              },
              error: function (data) {
                console.log(data);
              }
            })
          }

          $('#example').DataTable();

          $('.editstatusiklan').on('click', function(e) {
            e.preventDefault();
            var id = $(this).closest('tr').find('td .id_iklan').val();
            $.ajax({
              type : "POST",
              url:'<?php echo base_url() ?>user/checkoldpassword',
              data: {'id_iklan' : id},
              success: function (data) {
                console.log(data);

              },
              error: function (data) {
                console.log(data);
              }
            })
          })

          $('#verifikasi').on('submit', function (e) {
              e.preventDefault();
              var a = confirm('verifikasi iklan ini ?');
              if(a == true){
                  var id = $('#id_iklan').val();
                  $.ajax({
                      type: "GET",
                      url: "<?php echo base_url() ?>admin/verifikasiiklan/" + id,
                      success: function (data) {
                          if (data['status'] == "success"){
                              window.location.replace("<?php echo base_url() ?>admin");
                          }else{
                              location.reload();
                          }
                      },
                      error: function (data) {
                          location.reload();
                      }
                  })
              }
          })

          $('#buttoncancel').on('click', function (e) {
              e.preventDefault();
              var a = confirm('abaikan iklan ini ?');
              if(a == true){
                  var id = $('#id_iklan').val();
                  $.ajax({
                      type: "GET",
                      url: "<?php echo base_url() ?>admin/abaikaniklan/" + id,
                      success: function (data) {
                          if (data['status'] == "success"){
                              window.location.replace("<?php echo base_url() ?>admin");
                          }else{
                              location.reload();
                          }
                      },
                      error: function (data) {
                          location.reload();
                      }
                  })
              }
          })
			  });

		</script>
  </body>
</html>
