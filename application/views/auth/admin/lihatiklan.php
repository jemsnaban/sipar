<?php
  //var_dump($this->session->flashdata('message'));
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Home - Jualanjing</title>
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/bootstrap/css/bootstrap.min.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/styles/custom.css" media="screen" title="no title" charset="utf-8">
    <link href="<?php echo base_url() ?>/assets/styles/magnific-popup.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/plugins/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>/assets/styles/etalage.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>/assets/plugins/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
  </head>
  <body>
    <div class=" top-nav">
      <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url() ?>admin">TokoAnjing</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li><a href="<?php echo base_url() ?>admin">Home <span class="sr-only">(current)</span></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown active">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Welcome, <?php echo $this->session->userdata('admin_sipar')['username'] ?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo base_url() ?>login/logout">Keluar</a></li>
                </ul>
              </li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>

    </div>
    <!--container utama -->
    <div class="container main-container">
      <div class="row">
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url() ?>admin">Home</a></li>
          <li class="active">detail</li>
        </ol>
        <hr>
        <div class="dog-inner">
          <div class="col-md-9 col-sm-6 col-xs-12">
            <h4>Detail iklan</h4>
            <div class="row">
              <div class="col-md-5">
                <ul id="etalage">
                  <?php $det = $detail[0]; ?>
                  <?php foreach ($detail as $key): ?>
                    <li>
                      <img class="etalage_thumb_image" src="<?php echo base_url() ?>uploads/images/<?php echo $key['nama_gambar']; ?>" class="img-responsive" />
                      <img class="etalage_source_image" src="<?php echo base_url()?>uploads/images/<?php echo $key['nama_gambar']; ?>" class="img-responsive" />
                    </li>
                  <?php endforeach; ?>
                </ul>

                <video width="320" height="240" controls>
                  <source src="<?php echo base_url(); ?>uploads/videos/<?php echo $det['video']; ?>" type="video/mp4">
                  <source src="<?php echo base_url(); ?>uploads/videos/<?php echo $det['video']; ?>" type="video/ogg">
                  <source src="<?php echo base_url(); ?>uploads/videos/<?php echo $det['video']; ?>" type="video/3gp">
                  <source src="<?php echo base_url(); ?>uploads/videos/<?php echo $det['video']; ?>" type="video/webm">
                Your browser does not support the video tag.
                </video>

              </div>
              <div class="col-md-7 right-side">
                <h3><?php echo $det['judul_iklan'] ?></h3>
                <p>
                  <span><b>Lokasi:</b></span> <?php echo $det['nama_lokasi'] ?>,
                  <span>
                    <b>
                      <?php
                        $a = $det['tanggal_iklan'];
                        $new = DateTime::createFromFormat('Y-m-d', $a);
                        echo $new->format('d F Y');
                      ?>
                    </b>
                  </span>
                </p>
                <div class="overview">
                  <h3>IDR <?php echo number_format($det['harga'],0,",", ".") ?></h3>

                  <br><br>
                  <h4>Lahir </h4>
                  <?php
                    $myDateTime = DateTime::createFromFormat('Y-m-d', $det['tanggal_lahir']);
                    $newDateString = $myDateTime->format('d F Y');
                  ?>
                  <p><?php echo $newDateString; ?></p>
                  <h4>Deskripsi</h4>
                  <p>
                    <?php echo $det['deskripsi'] ?>
                  </p>
                </div>
                <!--div class="overview">
                  <button type="button" class="btn btn-success">Pesan / Hubungi Pemasang Iklan</button>
                </div-->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--akhir container utama -->

    <footer class="navbar-fixed-bottom">
      <div class="container">
        <div class="row">
          <center>Copyright @SIPAR 2016 </center>
        </div>
      </div>
    </footer>
    <script src="<?php echo base_url() ?>/assets/js/jQuery-2.2.0.min.js" charset="utf-8"></script>
    <script src="<?php echo base_url() ?>/assets/bootstrap/js/bootstrap.min.js" charset="utf-8"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.magnific-popup.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.etalage.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/plugins/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
    <script>
				jQuery(document).ready(function($){

          $('#etalage').etalage({
              thumb_image_width: 300,
              thumb_image_height: 300,

              show_hint: true,
              click_callback: function(image_anchor, instance_id){
                alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
              }
            });

          $('#err_pass').hide();$('#err_old_pass').hide();
          $('#pass').on('change', function () {
            checkPass();
          });
          $('#retype_pass').on('change', function () {
            checkPass();
          });

          function checkPass() {
            var pass = $('#pass').val();
            var retype = $('#retype_pass').val();

            if(pass != retype){
              //alert("no");
              $('#err_pass').show();
            }else{
              //alert("ues");
              $('#err_pass').hide();
            }
          }

          $('#oldpass').on('change', function () {
            cekOldPass();
          });

          function cekOldPass(){
            var arr = $('#oldpass').val();
            //alert($('#oldpass').val());
            $.ajax({
              type : "POST",
              url:'<?php echo base_url() ?>user/checkoldpassword',
              data: {'pass' : arr},
              success: function (data) {
                console.log(data);
                if (data['pass'] == 'diff') {
                  $('#err_old_pass').show();
                }else {
                  $('#err_old_pass').hide();
                }
              },
              error: function (data) {
                console.log(data);
              }
            })
          }

          $('#example').DataTable();

          $('.editstatusiklan').on('click', function(e) {
            e.preventDefault();
            var id = $(this).closest('tr').find('td .id_iklan').val();
            $.ajax({
              type : "POST",
              url:'<?php echo base_url() ?>user/checkoldpassword',
              data: {'id_iklan' : id},
              success: function (data) {
                console.log(data);

              },
              error: function (data) {
                console.log(data);
              }
            })
          })
			  });

		</script>
  </body>
</html>
