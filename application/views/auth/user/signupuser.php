<?php
  //var_dump($this->session->flashdata('message'));
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Home - Jualanjing</title>
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/bootstrap/css/bootstrap.min.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/styles/custom.css" media="screen" title="no title" charset="utf-8">
    <link href="<?php echo base_url() ?>/assets/styles/magnific-popup.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>/assets/styles/etalage.css" rel="stylesheet" type="text/css">
  </head>
  <body>
    <div class=" top-nav">
      <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url() ?>">TokoAnjing</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="active"><a href="<?php echo base_url() ?>">Home <span class="sr-only">(current)</span></a></li>
              <li><a href="<?php echo base_url() ?>home/pasangiklan">Pasang Iklan</a></li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>

    </div>
    <!--container utama -->
    <div class="container main-container">
      <div class="row">
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url() ?>">Home</a></li>
          <li class="active">Register User</li>
        </ol>
        <hr>
        <div class="dog-inner">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <h5>Jenis Anjing</h5>
            <ul class="list-group">
              <?php
                foreach ($jenis_anjing as $key) {
                  echo '<li class="list-group-item">
                    <span class="badge">'.$key['jlh'].'</span>
                    <a href="'.base_url().'home/jenis/'.$key['id_jenis'].'">'.$key['jenis_anjing'].'</a>
                  </li>';
                }
              ?>
            </ul>
          </div>
          <div class="col-md-9 col-sm-6 col-xs-12 login-user" style="padding-top:20px;">
            <div class="row">
              <div class="col-md-12 right-side">
                <h4>NEW CUSTOMERS</h4>
                <p>
                  If you have an account with us, please <a href="<?php echo base_url() ?>login">log in</a> .
                </p>
                <?php if ($this->session->flashdata('message') != NULL): ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?php echo $this->session->flashdata('message'); ?>
                  </div>
                <?php endif; ?>

                <form action="<?php echo base_url() ?>register/registeruser" method="post">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Username *</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="username" placeholder="Username" required="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">First Name *</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="first_name" placeholder="First Name" required="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Last Name *</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="last_name" placeholder="Last Name" required="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="pass">Password *</label>
                      <input type="password" class="form-control" id="pass" name="password" placeholder="Password" required="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="retype_pass">Retype Password *</label>
                      <input type="password" class="form-control" id="retype_pass" name="retype_password" placeholder="Retype Password" required="">
                      <p id="err_pass" style="color:red;">
                        * password doesn't match
                      </p>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Alamat *</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="alamat" placeholder="Alamat" required="">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Phone Number *</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="phone" placeholder="Phone Number" required="">
                    </div>
                    <button type="submit" class="btn btn-primary">Register</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--akhir container utama -->

    <footer>
      <div class="container">
        <div class="row">
          <center>Copyright @SIPAR 2016 </center>
        </div>
      </div>
    </footer>
    <script src="<?php echo base_url() ?>/assets/js/jQuery-2.2.0.min.js" charset="utf-8"></script>
    <script src="<?php echo base_url() ?>/assets/bootstrap/js/bootstrap.min.js" charset="utf-8"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.magnific-popup.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.etalage.min.js" type="text/javascript"></script>
    <script>
				jQuery(document).ready(function($){
					$('#etalage').etalage({
						thumb_image_width: 300,
						thumb_image_height: 400,

						show_hint: true,
						click_callback: function(image_anchor, instance_id){
							alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
						}
					});
          $('#err_pass').hide();

          $('#pass').on('change', function () {
            checkPass();
          });
          $('#retype_pass').on('change', function () {
            checkPass();
          });

          function checkPass() {
            var pass = $('#pass').val();
            var retype = $('#retype_pass').val();

            if(pass != retype){
              //alert("no");
              $('#err_pass').show();
            }else{
              //alert("ues");
              $('#err_pass').hide();
            }
          }
			});
		</script>
  </body>
</html>
