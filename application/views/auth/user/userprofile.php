<?php
  //var_dump($this->session->flashdata('message'));
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Home - Jualanjing</title>
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/bootstrap/css/bootstrap.min.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/styles/custom.css" media="screen" title="no title" charset="utf-8">
    <link href="<?php echo base_url() ?>/assets/styles/magnific-popup.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>/assets/styles/etalage.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/plugins/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>/assets/plugins/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
  </head>
  <body>
    <div class=" top-nav">
      <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url() ?>">TokoAnjing</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li><a href="<?php echo base_url() ?>">Home <span class="sr-only">(current)</span></a></li>
              <li><a href="<?php echo base_url() ?>home/pasangiklan">Pasang Iklan</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown active">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Welcome, <?php echo $this->session->userdata('user_sipar')['username'] ?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo base_url() ?>user/profile">Lihat Profile</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="<?php echo base_url() ?>login/logout">Keluar</a></li>
                </ul>
              </li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>

    </div>
    <!--container utama -->
    <div class="container main-container">
      <div class="row">
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url() ?>">Home</a></li>
          <li class="active">profile &raquo; <?php echo $this->session->userdata('user_sipar')['username']; ?></li>
        </ol>
        <hr>
        <div class="dog-inner">
          <div class="col-md-3 col-sm-4">
            <ul class="nav nav-pills nav-stacked">
              <li role="presentation" class="active"><a href="<?php echo base_url() ?>user/profile">Iklan Aktif</a></li>
              <li role="presentation"><a href="<?php echo base_url() ?>user/useriklan">Iklan blm Verified</a></li>
              <li role="presentation"><a href="<?php echo base_url() ?>user/usersettings">Pengaturan</a></li>
            </ul>
          </div>
          <div class="col-md-9 col-sm-8">
            <section class="content">
              <table id="example" class="display" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>Tanggal</th>
                          <th>Judul</th>
                          <th>Jenis</th>
                          <th>Harga</th>
                          <th>Deskripsi</th>
                          <th>Dilihat</th>
                          <th>Status Terjual</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody id="tbodyiklan">
                    <?php
                        if(!empty($iklan)){
                            foreach ($iklan as $key) {
                              echo '<tr>
                                  <td>'.$key['tanggal_iklan'].'</td>
                                  <td>'.$key['judul_iklan'].'</td>
                                  <td>'.$key['jenis_anjing'].'</td>
                                  <td>'.$key['harga'].'</td>
                                  <td>'.(strlen($key['deskripsi']) > 100 ? substr($key['deskripsi'], 0, 100) . "..." : $key['deskripsi'] ).'</td>
                                  <td><center>'.$key['hit'].'</center></td>
                                  <td><center>'.($key['status_jual'] == 1 ? "terjual" : "<a href='#' class='editstatusiklan'><i class='fa fa-edit' aria-hidden='true'></i></a>").'</center></td>
                                  <td>
                                    <center>
                                    <a href="'.base_url().'user/lihatiklan/'.$key['id_iklan'].'" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    <input type="hidden" class="id_iklan" value="'.$key['id_iklan'].'"/></center>
                                  </td>
                              </tr>';
                            }
                        }
                    ?>
                    </tbody>
                  </table>
            </section>
          </div>
        </div>
      </div>
    </div>
    <!--akhir container utama -->

    <footer class="navbar-fixed-bottom">
      <div class="container">
        <div class="row">
          <center>Copyright @SIPAR 2016 </center>
        </div>
      </div>
    </footer>
    <script src="<?php echo base_url() ?>/assets/js/jQuery-2.2.0.min.js" charset="utf-8"></script>
    <script src="<?php echo base_url() ?>/assets/bootstrap/js/bootstrap.min.js" charset="utf-8"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.magnific-popup.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.etalage.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/plugins/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
    <script>
				jQuery(document).ready(function($){
          $('#example').DataTable();

          $('#err_pass').hide();$('#err_old_pass').hide();
          $('#pass').on('change', function () {
            checkPass();
          });
          $('#retype_pass').on('change', function () {
            checkPass();
          });

          function checkPass() {
            var pass = $('#pass').val();
            var retype = $('#retype_pass').val();

            if(pass != retype){
              //alert("no");
              $('#err_pass').show();
            }else{
              //alert("ues");
              $('#err_pass').hide();
            }
          }

          $('#oldpass').on('change', function () {
            cekOldPass();
          });

          function cekOldPass(){
            var arr = $('#oldpass').val();
            //alert($('#oldpass').val());
            $.ajax({
              type : "POST",
              url:'<?php echo base_url() ?>user/checkoldpassword',
              data: {'pass' : arr},
              success: function (data) {
                console.log(data);
                if (data['pass'] == 'diff') {
                  $('#err_old_pass').show();
                }else {
                  $('#err_old_pass').hide();
                }
              },
              error: function (data) {
                console.log(data);
              }
            })
          }

          $('#tbodyiklan .editstatusiklan').on('click', function (e) {
              e.preventDefault();
              var a = confirm("ubah status iklan ?, action ini tidak dapat dibatalkan.");
              if (a == true){
                  var id = $(this).closest('tr').find('td .id_iklan').val();

                  $.ajax({
                      type: "GET",
                      url: "<?php echo base_url() ?>user/ubahstatusiklan/" + id,
                      success: function (data) {
                          if(data['status'] == "success"){
                              alert("data berhasil diubah");
                          }else{
                              alert("terdapat kesalahan");
                          }
                          //location.reload();
                      },
                      error: function (data) {
                          alert("terdapat kesalahan");
                          //location.reload();
                      }
                  })
              }
          })
        });

		</script>
  </body>
</html>
