-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 13, 2016 at 05:15 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sipar`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(10) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `password` varchar(60) NOT NULL,
  `role` enum('admin','user') NOT NULL DEFAULT 'admin'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id_admin`, `username`, `nama`, `password`, `role`) VALUES
(1, 'jems', 'jems naban', '26fe1b63037fe9b3dcd06fd6422f385d08b90663', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `gambar_iklan`
--

CREATE TABLE `gambar_iklan` (
  `id_gambar` int(11) NOT NULL,
  `id_iklan` int(11) NOT NULL,
  `nama_gambar` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gambar_iklan`
--

INSERT INTO `gambar_iklan` (`id_gambar`, `id_iklan`, `nama_gambar`) VALUES
(1, 1, 'akitadua.jpg'),
(2, 1, 'akitatiga.jpg'),
(3, 2, 'alaskandua.jpg'),
(4, 2, 'alaskantiga.jpg'),
(5, 3, 'dobermandua.jpg'),
(6, 3, 'dobermantiga.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `iklan`
--

CREATE TABLE `iklan` (
  `id_iklan` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `harga` int(11) NOT NULL,
  `id_lokasi` int(11) NOT NULL,
  `judul_iklan` text NOT NULL,
  `deskripsi` text NOT NULL,
  `gambar` text NOT NULL,
  `video` text NOT NULL,
  `tanggal_iklan` date NOT NULL,
  `hit` int(11) NOT NULL,
  `status_jual` int(11) NOT NULL DEFAULT '0',
  `tanggal_jual` date NOT NULL,
  `verified` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iklan`
--

INSERT INTO `iklan` (`id_iklan`, `id_user`, `id_jenis`, `tanggal_lahir`, `harga`, `id_lokasi`, `judul_iklan`, `deskripsi`, `gambar`, `video`, `tanggal_iklan`, `hit`, `status_jual`, `tanggal_jual`, `verified`) VALUES
(1, 2, 1, '2016-08-02', 2000000, 1, 'Anakan American Akita Jantan Bagus', 'Bapak : Ina Ch Estava Rain Foreign Affair ( Import )\nInduk : Dynamic Force Can''t Buy My Love ( Import )\n\nAnakan American Akita SQ, Tulang Kasar, Kepala Besar, Karekter Bagus, Berani.\nStambum sudah jadi, Vaksin E 6 .\n\nSERIUS!!! Bandingkan dan Buktikan Baru Beli.\nMINAT!!! HUBUNGIN SAYA. MAAF SMS TIDAK AKAN SAYA TANGGAPI	', 'akitasatu.jpg', 'funny.mp4', '2016-11-13', 15, 0, '0000-00-00', 1),
(2, 2, 2, '2016-04-01', 1800000, 2, 'Alaskan Malamute Betina', 'Sudah tatto (stambum dl proses Perkin)\nVaksin	\n\nLokasi di Bandung	\n', 'alaskansatu.jpg', 'funny.mp4', '2016-11-13', 5, 0, '0000-00-00', 1),
(3, 2, 4, '2016-03-17', 3000000, 1, 'Doberman Berkwalitas', 'Anda sedang mencari ANAKAN Doberman BETINA BERKWALITAS... ???\n\nName : Alexia Putellas Von Tiffany Givens\nSex : Female\nDOB : 10 June 2016\n\nBapak : Get To The Point Ego ( Import Roma ) ----> Destiny''s Phoenix Highway To Hell X CH. Get To The Point Berta\n\nIbu : Eye Of Garuda Ambara ( INA. CH. Harmonic Bryant Von Basilius X Astra Von Kharisma )\n\nDoberman betina ini memiliki blood line yang bagus, anatomi yang baik, memiliki kaki dan kepala yang kasar, didukung dengan siku - siku yang baik, sangat lincah dan sehat dan terawat. Fostur tubuh yang besar, bisa untuk show, dan juga cocok untuk jaga.\n\nPemberian obat cacing yang rutin, Dog Food, Vitamin dan Kalsium yang berkwalitas, Vaksin E4 dan Stamboon sudah jadi.\n\nSo..... jangan anda ragu - ragu, jika anda ingin yang BERKWALITAS... ??? Silahkan hubungi ke nomor ini sekarang juga :\n\n0813 1109 0971, 08170710369\n\nMohon maaf... kami hanya melayani penelepon langsung yang serius, tidak melayani SMS / WA apa lagi minta kirim- kirim video / foto. GBU	', 'doberman.jpg', 'funny.mp4', '2016-11-13', 1, 1, '2016-11-13', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL,
  `jenis_anjing` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `jenis_anjing`) VALUES
(1, 'Akita'),
(2, 'Alaskan Malamute'),
(3, 'Chinese Shar-pei'),
(4, 'Doberman Pinscher'),
(5, 'Standard Schnauzer'),
(6, 'Siberian Husky'),
(7, 'Pembroke Welsh Corgi'),
(8, 'Old English Sheepdog'),
(9, 'Miniature Schnauzer'),
(10, 'Labrador Retriever'),
(11, 'German Shepherd'),
(12, 'Italian Greyhound'),
(13, 'West Highland White Terrier'),
(14, 'Corgy');

-- --------------------------------------------------------

--
-- Table structure for table `lokasi`
--

CREATE TABLE `lokasi` (
  `id_lokasi` int(11) NOT NULL,
  `nama_lokasi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lokasi`
--

INSERT INTO `lokasi` (`id_lokasi`, `nama_lokasi`) VALUES
(1, 'Jakarta'),
(2, 'Bandung');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(10) NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `password` varchar(50) NOT NULL,
  `no_hp` varchar(14) NOT NULL,
  `alamat` text NOT NULL,
  `date_join` date NOT NULL,
  `role` enum('admin','user') NOT NULL DEFAULT 'user'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `first_name`, `last_name`, `password`, `no_hp`, `alamat`, `date_join`, `role`) VALUES
(1, 'aditya', 'Aditya Okke', 'Sugiarso', '6d08342db9799d57f02751a9283ab6609efda25d', '081315230667', 'Bandung, California', '2016-06-16', 'user'),
(2, 'user', 'user', 'biasa', 'f92e7f3a5d37658f2664e16033d255b2607b6f2e', '098080807', 'Jakarta Raya, Jl. Cendana', '2016-06-16', 'user'),
(3, 'admin', 'super', 'admin', 'f92e7f3a5d37658f2664e16033d255b2607b6f2e', '081332234587', 'sagan timur ct V no 8A, Sleman, Yogyakarta', '2016-11-01', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `gambar_iklan`
--
ALTER TABLE `gambar_iklan`
  ADD PRIMARY KEY (`id_gambar`);

--
-- Indexes for table `iklan`
--
ALTER TABLE `iklan`
  ADD PRIMARY KEY (`id_iklan`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`id_lokasi`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `gambar_iklan`
--
ALTER TABLE `gambar_iklan`
  MODIFY `id_gambar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `iklan`
--
ALTER TABLE `iklan`
  MODIFY `id_iklan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `lokasi`
--
ALTER TABLE `lokasi`
  MODIFY `id_lokasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
